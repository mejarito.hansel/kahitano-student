$ = (typeof $ !== 'undefined') ? $ : {};
$.magsaysay = (typeof $.magsaysay !== 'undefined') ? $.magsaysay : {};

$.magsaysay.portal = (typeof $.magsaysay.portal !== 'undefined') ? $.magsaysay.portal : {};

$.magsaysay.portal = (function () {
var host = window.location.protocol+"//"+window.location.hostname ;

var API = host+'/api2/wsv1/api/student/'; 

	var __Authentication = function() {
	$("#loginAlert").hide();
	$("#loginSuccess").hide();


	$('#txtPassword').keypress(function (e) {
		var key = e.which;
		if(key == 13) 
		{
		$('#btnLogin').click();
		return false;
		}
});
	$("#btnLogin").on('click', function () {

		let payload = {
			method: "authenticate",
			username :  $("#textFullname").val(),
			birthday: "",
			password:  $("#txtPassword").val()
		}

			if($("#textFullname,#txtPassword,#txtDate").val()===""){
			// if($("#textFullname,#txtPassword").val()===""){

				$("#textFullname").addClass("is-invalid");
				$("#txtDate").addClass("is-invalid");
				$("#txtPassword").addClass("is-invalid");
				$('.labelText').addClass("text-danger");
				$("#textFullname,#txtPassword").attr("placeholder","");
				$("#loginAlert").show();

			}else if($("#textFullname").val()===""){

				$("#textFullname").addClass("is-invalid");

			}else if($("#txtPassword").val()===""){sss

				$("#txtPassword").addClass("is-invalid");

			}
		else{
              $.magsaysay.executeExternalPost(API, JSON.stringify(payload)).done(function(result){
              	//console.log(result.status)
              	//console.log(payload)
                  if(result.status === "SUCCESS"){
                	console.log(result.payload.si_ID)
					$("#loginSuccess").show();
					setTimeout(function(){
		  	 		console.log(payload)
		  	 		$.cookie("student_id",$("#textFullname").val())
		  	  		$.cookie("stud_id", result.payload.si_ID)	
		  	 		window.location.href = $.magsaysay.getContextPath()+"student/home";	 
		  	 		}, 200);	
		 		 	}else{
		 		 			$("#loginAlert").show();
		 		 	}


				  })	

		}
		

})}

	var __GetInfo = function () {

		let payload = {
		method: "student_info",
		si_ID: $.cookie("stud_id")
		}
        $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){
	        if(result.status === "SUCCESS"){
	        //console.log(result.payload)
	        $("#txtStudentId").html($.cookie("student_id"))
	        $("#txtStudentName").html(result.payload.si_FNAME +" "+result.payload.si_MNAME.charAt(0)+". "+result.payload.si_LNAME)
	        }
    	})

}

	var __GetAdditionalInfo = function () {

		let payload = {
		method: "student_additional_info",
		si_ID: $.cookie("stud_id")
		}
        $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){
        if(result.status === "SUCCESS"){
      //  console.log(result.payload)
        }
        	})

}

	var __GetCurriculum = function () {

		let payload = {
		method: "curriculum",
		si_ID: $.cookie("stud_id")
		}
        $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){
        if(result.status === "SUCCESS"){
    //    console.log(result.payload)
        $("#txtCourseP").html(result.payload.course_NAME)
       // $("#txtStudentId")
        }
        	})

}
	var __GetSemofStudents = function () {

		let payload = {
		method: "getallsemsofstudent",
		si_ID: $.cookie("stud_id")
		}
        $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){
            if(result.status == "SUCCESS"){
                $("#SemofStudent").empty().append('<option>SELECT SEMESTER</option>');
                for(i=0;i<result.payload.length;i++){

                    $("#SemofStudent").append($('<option>',{
                        value : result.payload[i].sem_ID,
                        text : result.payload[i].sem_NAME
                    },'</option'));

                }

                
            }else{

            }
        })

}

	var __GetEnrolledSubject = function () {

		$("#SemofStudent").on('change', function () {

			let payload = {
			method: "enrolled_subject",
			si_ID: $.cookie("stud_id"),
			sem_ID:$("#SemofStudent").val()
				}

	        $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){
		        if(result.status === "SUCCESS"){
		        	  console.log(result)
		        	  if ($.fn.DataTable.isDataTable('#SubjectEncodedTable')) {
				       $('#SubjectEncodedTable').DataTable().destroy();
				       $('#SubjectEncodedTable tbody').empty();
				     }
				            var arr = [];
					       // result.payload.forEach(function(test){
					        // var obj = {};
					        for (i=0;i<result.payload.length;i++){
					          var obj = {};
					          var item = result.payload[i];     
					          obj['day'] = item.day;
					          obj['RM1'] = item.RM1;
  				              obj['schedule'] = item.start+" - "+item.end;
  				              obj['subject_CODE'] = item.subject_CODE+" - "+item.subject_DESCRIPTION;
  				              obj['semester_section_NAME'] = item.semester_section_NAME;

					          //console.log(obj)        

					           arr.push(obj);  
					           
					         }

					       $(document).ready( function () {	
					         $('#SubjectEncodedTable').DataTable({
					            'data' : arr,
					            'columns': [{"data": "day"}, {"data": "RM1"}, {"data": "schedule"},{"data": "subject_CODE"},{"data": "semester_section_NAME"} ]
					         });
					      		
					     } );


					     }
	        	})
	        })

}
// $('.CONTAINER').append("<table class='dynamic' + > <table>")
	var __GetCurricullumSubjects = function(){
		
		let payload = {
			method: "getcurricullum",
			si_ID: $.cookie("stud_id")
		}

		 $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){ 

		  for(i=0;i<result.payload.CurricullumSubject.length;i++){
		  	var item = result.payload.CurricullumSubject[i];
				$('.tableContainer').append('<table class="table table-bordered table_'+i+'"> <thead> <tr><th colspan="3">'+item.SemesterName+'</th></tr><tr><th width="20%">Course Code</th><th width="65%">Description</th><th>Pre-Req</th></tr></thead><tbody></tbody></table>');
				
				for (j=0;j<item.SemesterSubject.length;j++){
					var item1 = item.SemesterSubject[j];
					var curr = "";
					if(item1.prerequisitesubj == null)
					{
						curr = "-";

					}else
					{
						curr=item1.prerequisitesubj;
					}
					$(".table_"+i+" tbody").append('<tr> <td> '+ item1.subject_CODE+' </td> <td>'+ item1.subject_DESCRIPTION+'</td><td>'+curr+'</td></tr>');
				}
		
		  }
		 });

	};

	var __GetEncodedGrades = function () {
		$("#SemofStudent").on('change', function () {

			let payload = {
			method: "encoded_grades",
			si_ID: $.cookie("stud_id"),
			sem_ID:$("#SemofStudent").val()
				}

	        $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){
		        if(result.status === "SUCCESS"){
	        	  	console.log(result.payload)
		        	  if ($.fn.DataTable.isDataTable('#SubjectEncodedTable')) {
				       $('#SubjectEncodedTable').DataTable().destroy();
				       $('#SubjectEncodedTable tbody').empty();
				     }
				            var arr = [];
					       // result.payload.forEach(function(test){
					        // var obj = {};
					        for (i=0;i<result.payload.length;i++){
					          var obj = {};
					          var item = result.payload[i];
					          var check ='';
					          if(item.is_check == 0){
					          		check = " ";
					          	}else{
					          		check = item.grade;

					          	}				             
					          obj['subject_CODE'] = item.subject_CODE;
					          obj['subject_DESCRIPTION'] = item.subject_DESCRIPTION;
  				              obj['grade_mid'] = item.grade_mid;
  				              obj['grade'] = check;
  				              obj['semester_section_NAME'] = item.semester_section_NAME;
					          //console.log(obj)        

					           arr.push(obj);
					           
					         }

					       $(document).ready( function () {	
					      			 // console.log(result.payload.length)
					      			  // for (j=0;j<result.payload.length;j++){
					      		 		// console.log(arr[j].grade)
					      			 	//   if(arr[j].grade == "DRP "){
					      			 	//   	$("tr td:nth-child(3)").css({"background-color": "red"});
					      			 	//   }
						      			 // 	  }
					         $('#SubjectEncodedTable').DataTable({
					            'data' : arr,
					            'columns': [{"data": "subject_DESCRIPTION"}, {"data": "subject_CODE"}, {"data": "grade_mid"},{"data": "grade"},{"data": "semester_section_NAME"} ]
					         });

					         for(i=0;i<$("#SubjectEncodedTable tr").length;i++){
					         console.log($("#SubjectEncodedTable tr td:nth-child(3)").val() + " ");
					        // if ($("#SubjectEncodedTable tr td:nth-child(3)").text() == "DRP"){
					        // 	$(this).css({"background-color":"red"})
					       //  }
					        // if($("#SubjectEncodedTable tr td:nth-child(2)").)
					         }
   
					     } );


					     }
	        	})
	        })

}
return{

	Authentication : __Authentication,
	GetInfo : __GetInfo,
	GetAdditionalInfo: __GetAdditionalInfo,
	GetCurriculum : __GetCurriculum,
	GetSemofStudents : __GetSemofStudents,
	GetEnrolledSubject : __GetEnrolledSubject,
	GetEncodedGrades :__GetEncodedGrades,
	GetCurricullumSubjects:__GetCurricullumSubjects
}
	


}());
