$ = (typeof $ !== 'undefined') ? $ : {};
$.magsaysay = (typeof $.magsaysay !== 'undefined') ? $.magsaysay : {};

$.magsaysay = (function () {

	var __getContextPath = function () {
		//return "/dokyumento-portal/index.php/";
		//return "/index.php/";
		return "/";
	};

	var __getImgPath = function () {
		//return "/dokyumento-portal/";
		return "/";
	};
	var __executeGet = function (path) {
		var dfd = $.Deferred();
		$.get(path, function (data) {})
			.done(function (data) {
				dfd.resolve(data);
			})
			.fail(function (qXHR, textStatus, errorThrown) {
				dfd.resolve({
					status: 'ERROR',
					message: errorThrown
				});
			})
			.always(function (data) {

			});
		return dfd.promise();
	};
	var __executePost = function (path, jsonObj, customLoader) {
		path = $.magsaysay.getContextPath() + path;
		var d = $.Deferred();

		if (customLoader != "") {
			var $loader = $("#" + customLoader),
				timer;
		}

		$.ajax({
			method: "POST",
			url: path,
			dataType: "json",
			data: jsonObj,
			beforeSend: () => {
				if (customLoader != "") {
					timer && clearTimeout(timer);
					timer = setTimeout(() => $loader.show(), 100);
				}
			}
		}).done(function (data, textStatus, jqXHR) {
			if (customLoader != "") {
				clearTimeout(timer);
				$loader.hide();
			}

			d.resolve(data);
		}).fail(function (jqXHR, textStatus, errorThrown, request) {
			console.log('---FAILED---');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log('---FAILED---');

			d.resolve({
				status: 'ERROR',
				message: request
			});

			if (customLoader != "") {
				$("#" + customLoader).hide();
			}
		});

		return d.promise();
	};

	var __executeExternalPost = function (path, jsonObj, customLoader) {
		// path = $.dokyumento.getContextPath() + path;
		var d = $.Deferred();
		if (customLoader != "") {
			$("#" + customLoader).show();
		}
		$.ajax({
			method: "POST",
			url: path,
			dataType: "json",
			data: jsonObj
		}).done(function (data, textStatus, jqXHR) {
			if (customLoader != "") {
				$("#" + customLoader).hide();
			}
			d.resolve(data)
		}).fail(function (jqXHR, textStatus, errorThrown, request) {
			console.log('---FAILED---');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log('---FAILED---');

			d.resolve({
				status: 'ERROR',
				message: request
			});

			if (customLoader != "") {
				$("#" + customLoader).hide();
			}
		});

		return d.promise();
	};

	var __executeFile = function(path, jsonObj) {
		var d = $.Deferred();
				$(".overlay-back").show();
				$(".loadDiv").show();
		$.ajax({
				method: "POST",
				url: path,
				dataType: "json",
				cache: false,
				processData: false,
				contentType: false,
				/*data: JSON.stringify(jsonObj)*/
				data: jsonObj
		}).done(function (data, textStatus, jqXHR) {
				d.resolve(data);
				$(".loadDiv").hide();
				$(".overlay-back").hide();
		}).fail(function (jqXHR, textStatus, errorThrown) {
				console.log('---FAILED---');
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				console.log('---FAILED---');
				
				d.resolve({
						status : 'ERROR',
						message : errorThrown
				});
				$(".overlay-back").hide();
				$(".loadDiv").hide();
		});
		return d.promise();
};


	var __executeBatchFile = function (path, jsonObj) {
		var d = $.Deferred();
		$(".overlay-back").show();
		$(".loadDiv").show();
		var key = jsonObj.get('file_id');
		// console.log(jsonObj.get('file_id'));
		$.ajax({
			method: "POST",
			url: path,
			dataType: "json",
			cache: false,
			processData: false,
			contentType: false,
			/*data: JSON.stringify(jsonObj)*/
			data: jsonObj,
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				//Upload progress
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						//Do something with upload progress
						var percent = Math.round((percentComplete) * 100);
						// console.log(percent);
						// $("#progress-bar-percentage").css('width', percent * 5);
						// $("#percentageUpload").html(percent + "%").css({
						// 	'text-align': 'center',
						// 	'padding-bottom': '300px'
						// });
					
						$(".batchPercentage" + key).html(percent + "%");
						$(".batchPercentage" + key).css({
							'font-size': "12px",
							'padding-left' : '8%'						
						});
						// $(".fa fa-2x indicator0 fa-check-circle success").css({
							
						// });
					}
				}, false);

				return xhr;
			},
		}).done(function (data, textStatus, jqXHR) {
			d.resolve(data);
			$(".loadDiv").hide();
			$(".overlay-back").hide();
		}).fail(function (jqXHR, textStatus, errorThrown) {
			console.log('---FAILED---');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log('---FAILED---');

			d.resolve({
				status: 'ERROR',
				message: errorThrown
			});
			$(".overlay-back").hide();
			$(".loadDiv").hide();
		});
		return d.promise();
	};

	var __executeSingleFile = function (path, jsonObj) {
		var d = $.Deferred();
		$(".overlay-back").show();
		$(".loadDiv").show();
		var key = jsonObj.get('file_id');
		// console.log(jsonObj.get('file_id'));
		$.ajax({
			method: "POST",
			url: path,
			dataType: "json",
			cache: false,
			processData: false,
			contentType: false,
			/*data: JSON.stringify(jsonObj)*/
			data: jsonObj,
			xhr: function () {
				var xhr = new window.XMLHttpRequest();
				//Upload progress
				xhr.upload.addEventListener("progress", function (evt) {
					if (evt.lengthComputable) {
						var percentComplete = evt.loaded / evt.total;
						//Do something with upload progress
						var percent = Math.round((percentComplete) * 100);
						// console.log(percent);
						$("#progress-bar-percentage").css('width', percent * 5);
						$("#percentageUpload").html(percent + "%").css({
							'text-align': 'center',
							'padding-bottom': '300px'
						});
					
						// $(".batchPercentage" + key).html(percent + "%");
						// $(".batchPercentage" + key).css({
						// 	'font-size': "10px",
						// 	"margin-left": "10px"
						// });
						// $(".fa fa-2x indicator0 fa-check-circle success").css({
						// 	'margin-left': "10px"
						// });
					}
				}, false);

				return xhr;
			},
		}).done(function (data, textStatus, jqXHR) {
			d.resolve(data);
			$(".loadDiv").hide();
			$(".overlay-back").hide();
		}).fail(function (jqXHR, textStatus, errorThrown) {
			console.log('---FAILED---');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log('---FAILED---');

			d.resolve({
				status: 'ERROR',
				message: errorThrown
			});
			$(".overlay-back").hide();
			$(".loadDiv").hide();
		});
		return d.promise();
	};


	return {
		getContextPath: __getContextPath,
		getImgPath: __getImgPath,
		executePost: __executePost,
		executeExternalPost: __executeExternalPost,
		executeGet: __executeGet,
		executeFile: __executeFile,
		executeBatchFile : __executeBatchFile,
		executeSingleFile : __executeSingleFile
	};
}());