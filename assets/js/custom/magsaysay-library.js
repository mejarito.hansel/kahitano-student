

$ = (typeof $ !== 'undefined') ? $ : {};
$.magsaysay = (typeof $.magsaysay !=='undefined')? $.magsaysay: {};
$.magsaysay.library = (typeof $.magsaysay.library !=='undefined')? $.magsaysay.library : {};

$.magsaysay.library =(function() {
  var host = window.location.protocol+"//"+window.location.hostname ;
    var API = host+'/api2/wsv1/api/student/';
    var path = host+"/api2/wsv1/ApiLibrary/";
    var pathmagsaysay = host+"/api/index.php/Ams_controller/"
    var __attachEventsToMaterials = function() {
      console.log("attached events to library");

      $.magsaysay.executeExternalPost(path +'fetchAllMaterials2').done(function (result) {
      // $.magsaysay.executeExternalPost(path +'fetchallrequest').done(function (result) {
        console.log(result);

        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].DATE_ACCESSION));
           trc.append($('<td class="row2">')
             .append(result.payload[i].ACCESSION_NO));
           trc.append($('<td class="row3">')
             .append(result.payload[i].MATERIAL_TITLE));
           trc.append($('<td class="row5">')
             .append(result.payload[i].BOOK_TYPE));
           trc.append($('<td class="row6">')
             .append(result.payload[i].AUTHOR_NAME));
           trc.append($('<td class="row7">')
             .append(result.payload[i].MATERIAL_COPYRIGHT));
           trc.append($('<td class="row8">')
             .append(result.payload[i].MATERIAL_SOURCE));

           var bookStatus = result.payload[i].BOOK_STATUS
             if (bookStatus == "" || bookStatus == null) {
              bookStatus = "Available"
             }else if((bookStatus === "Returned")||(bookStatus === "Expired")) {
              bookStatus = "Available"
             }else{
              bookStatus = result.payload[i].BOOK_STATUS
             }
           trc.append($('<td class="row4">')
             .append(bookStatus));

            var borrowed;
              if(bookStatus === "Request to borrow"){
                borrowed = "hide";
              }
              else{
                borrowed = "";
              }

           trc.append($('<td class="row5">')
             .append($('<button class="btn btn-primary btn-xs update '+borrowed+'" '
                  + ' data-uid="'+ result.payload[i].MATERIAL_ID +' "  >Request to borrow</button>')))

           $('#tbody').append(trc);

          }

        var dataRow = $("#myTable").DataTable({

          'destroy': true,
          "scrollY": true,
          "bFilter": false,
          "bLengthChange": false

        });

        $('.update').unbind('click').on('click',function(){
            $("#myModalupdate").modal();
            MATERIAL_ID = $(this).data('uid')
            console.log('MATERIAL_ID ' + '= ' + MATERIAL_ID)

        });

        $('#updatebtn').unbind('click').on('click',function(){

          let payload = {
          method: "student_info",
          si_ID: $.cookie("stud_id")
          }

          $.magsaysay.executeExternalPost(API,JSON.stringify(payload)).done(function(result){
            if(result.status === "SUCCESS"){
                //console.log(result.payload)
                
              var studentBname = result.payload.si_FNAME +" "+result.payload.si_MNAME.charAt(0)+". "+result.payload.si_LNAME
              var payload = {
                "RL_ID" : studentBname,
                "MATERIAL_ID" : MATERIAL_ID,
                "B_DATE_BORROWED" : "0000-00-00 00:00:00",
                "B_DUE_DATE" : "0000-00-00 00:00:00",
                "B_DATE_RETURNED" : "",
                "B_FINES" : "",
                "BOOK_STATUS_ID" : "2"
              }

              $.magsaysay.executeExternalPost(path + "insert_borrow_returned", JSON.stringify(payload)).done(function(result) {
                console.log(result)
                var status = result.status;
                
                if(status == "SUCCESS") {
                    // console.log("complete");
                    $("#success").removeClass("hide");
                    setTimeout(function() {
                    window.location.href = "library_search"
                    },200);

                }else {
                    // console.log("Failed");
                    $("#submitCC").removeClass("hide");
                    $("#preloader").addClass("hide");
                    $("#icpID").removeClass("hide");
                    $("#submitAdminPass").removeClass("hide");
                }
              })
            }
          })
        })
      })
    }

    var __changePass = function(){
      console.log("test")
      $('#change').unbind('click').on('click',function(){
        $("#icpID").addClass("hide");
        $("#success").addClass("hide");
        $("#requiredfield").addClass("hide");
        $("#passwordmatch").addClass("hide");
        $("#newPass").val("");
        $("#retypePass").val("");


      })
      $('#saveBtn').unbind('click').on('click',function(){
        var password = $("#newPass").val()
        var verifyPassword = $("#retypePass").val()
        if (password.length > 0 && verifyPassword.length > 0) {
            $("#requiredfield").addClass("hide");
            $("#passwordmatch").addClass("hide");

            if (password == verifyPassword) {
                // console.log("success");
                $("#preloader").removeClass("hide");
                $("#passwordmatch").addClass("hide");
                $("#submitAdminPass").addClass("hide");

                var payload = {
                    "si_ID" : $.cookie("stud_id"),
                    "password" : verifyPassword,
                }
                console.log(payload);
                  $.magsaysay.executeExternalPost(pathmagsaysay + "updateStudentPassword", JSON.stringify(payload)).done(function(result) {
                    console.log(result);
                    var status = result.status;
                    // console.log(result.workgroup_id);

                    if(status == "SUCCESS") {
                        // console.log("complete");
                        $("#success").removeClass("hide");
                        // setTimeout(function() {
                        // window.location.href = "registered_applicants.php"
                        // },200);

                    }else {
                        // console.log("Failed");
                        $("#submitCC").removeClass("hide");
                        $("#preloader").addClass("hide");
                        $("#icpID").removeClass("hide");
                        $("#submitAdminPass").removeClass("hide");
                    }
                  })
                
            }else{
                // console.log("failed");
                $("#passwordmatch").removeClass("hide");
            }

        }else {
            // console.log("required");
            $("#requiredfield").removeClass("hide");
            $("#passwordmatch").addClass("hide");
        }
      })
    }


    return {
        attachEventsToMaterials : __attachEventsToMaterials,
        changePass : __changePass
    }


}());
