<?php $this->load->view('templates/navbartop.php'); ?>
<?php $this->load->view('templates/preloader'); ?>
<div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Fullname</label>
						<div class="col-sm-10">
						<input type="text" class="form-control form-control-sm" id="textFullname" placeholder="Fullname">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Email Address</label>
						<div class="col-sm-10">
						<input type="text" class="form-control form-control-sm" id="" placeholder="Email Address">
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Gender</label>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
							<label class="form-check-label small" for="inlineRadio1">Male</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
							<label class="form-check-label small" for="inlineRadio2">Female</label>
						</div>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Birthdate</label>
						<div class="col-sm-10">
						<input type="text" class="form-control form-control-sm" id="" placeholder="10/15/1994" readonly>
						</div>
					</div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnSave" type="button" class="btn btn-sm btn-block btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-top:100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="card profile text-center" style="width: 18rem;">
                    <span class="mini-button">
                    <img src="<?php echo base_url(); ?>/assets/img/sample-picture.png" style="width:200px; height:200px;" class="rounded-circle mx-auto d-block " alt="Cinque Terre">
                    </span>
                    <div class="card-body">
                        <h5 class="card-title" id ="txtStudentName"></h5>
                        <p class="card-text small">
                        	Program:
                        	<span id="txtCourseP"></span>
                    	</p>
                        <p class="card-text small">Cadet No: <span id="txtStudentId"></span></p>
                        <hr>
                       <!--  <a href="#" id="btnEditProfile" class="btn btn-sm btn-block btn-primary">Edit Profile</a> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-8">

                <div class="col-lg-12">
                   <?php $this->load->view('templates/menubar.php'); ?>

                </div>

                <div class="col-lg-12" style="margin-top:20px; padding-top:20px;">
                    <h4 class="text-uppercase section-header">Latest New's</h4><br>
                    <div class="card" style="padding:0px 30px 20px 30px !important;" id="right-section-body">
                        <ul class="list-unstyled" style="margin-top:30px;">
                            <li class="media my-4">
                                <img class="mr-3" style="width:300px;" src="<?php echo base_url(); ?>/assets/img/pexels-photo-207569.jpeg" alt="Generic placeholder image">
                                <div class="media-body small">
                                    <p class="h6 font-weight-bold">H.T.M.L's News and Announcements, High School, Middle School</p>
                                    <p class="mt-0 mb-1 text-mute">Posted by <span class="font-weight-bold" style="color:black">Abdul Johul</span><br> Published:  <span class="font-weight-bold" style="color:black">April 25, 2018</span><br></p><br>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    <a class="text-right small" href="">Read more...</a>
                                </div>
                            </li>
                            <li class="media my-4">
                                <img class="mr-3" style="width:300px;" src="<?php echo base_url(); ?>/assets/img/library-la-trobe-study-students-159775.jpeg" alt="Generic placeholder image">
                                <div class="media-body small">
                                    <p class="h6 font-weight-bold">P.H.P's News and Announcements, High School, Middle School</p>
                                    <p class="mt-0 mb-1 text-mute">Posted by <span class="font-weight-bold" style="color:black">Konjo Panjita</span><br> Published:  <span class="font-weight-bold" style="color:black">April 25, 2018</span><br></p><br>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    <a class="text-right small" href="">Read more...</a>
                                </div>
                            </li>
                            <li class="media my-4">
                                <img class="mr-3" style="width:300px;" src="<?php echo base_url(); ?>/assets/img/people-woman-coffee-meeting.jpg" alt="Generic placeholder image">
                                <div class="media-body small">
                                    <p class="h6 font-weight-bold">JavaScript's News and Announcements, High School, Middle School</p>
                                    <p class="mt-0 mb-1 text-mute">Posted by <span class="font-weight-bold" style="color:black">Chiri Chiri Kong</span><br> Published:  <span class="font-weight-bold" style="color:black">April 25, 2018</span><br></p><br>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                    <a class="text-right small" href="">Read more...</a>
                                </div>
                            </li>
                            
                        </ul>
                        <a class="text-right small" href="">See all news...</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('templates/footer.php'); ?>
<script type="text/javascript">
    $.magsaysay.portal.GetInfo();
    $.magsaysay.portal.GetAdditionalInfo();
    $.magsaysay.portal.GetCurriculum();
    // $.magsaysay.portal.GetSemofStudents();
    // $.magsaysay.portal.GetEnrolledSubject();
    // $.magsaysay.portal.GetEncodedGrades();
</script>