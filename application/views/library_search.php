<?php $this->load->view('templates/navbartop.php'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap4.min.css">
<div class="modal fade" id="myModalupdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <p class="h4"><b>Borrow Book Tool</b></p>
                            <p class="text-muted small">"There will be only 1 day - change it to one Book Reservation of book is only valid for 1 day.
                                Failure to do so will result to cancellation.”</p>
                            <p></p>
                        </div>
                    </div>
                    <div class='col-lg-10' style='margin:0 auto;'>
                        <!-- <div class="form-group">
                            <input type='date' class="form-control" id='borrowdate' placeholder="Borrow Date" />
                            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="form-group">
                            <input type='date' class="form-control" id='returndate' placeholder="Return Date" />
                            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
                        </div> -->
                        <div class="form-group">
                            <input type="button" id="updatebtn" class="btn btn-block btn-success" value="Save">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" style="margin-top:6rem;">
    <div class="container"><br>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-4">
                        <h1 class="magsaysay-font-color" style="width: 90%;font-family:'Times New Roman','serif'; margin-top: 0px;">E-Library Result</h1>
                    </div>
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label for="txtSearchBar">
                            </label><input id="txtSearchBar-sm" type="text" class="form-control" placeholder="Search by Book Name, Book Author, Book Type" onkeyup="myFunction()" style="padding-left:1rem;width: 83%;float: left;"> <input type="submit" class="btn btn-primary" value="Search Book" id="searchBtn" onclick="myFunction()">
                        </div>
                         <div class="form-group">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name='filterby' id="filter-1" value="BName">
                                <label class="form-check-label text-muted" for="filter-1"> Filter by Book Name</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name='filterby' id="filter-2" value="BAuthor">
                                <label class="form-check-label text-muted" for="filter-2"> Filter by Book Author</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name='filterby' id="filter-3" value="BType">
                                <label class="form-check-label text-muted" for="filter-3"> Filter by Book Type</label>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div><br>
        <div class='row'>
            <div class="col-md-12">
                <div class='table-reponsive'>
                    <table id="myTable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Date accession no.</th>
                                <th>Accession no.</th>
                                <th>Book Name</th>
                                <th>Book Type</th>
                                <th>Book Author</th>
                                <th>Copyright</th>
                                <th>Source</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('templates/footer.php'); ?>
<script type="text/javascript" src='<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js'>
</script>
<!-- <script type="text/javascript" src='<?php echo base_url(); ?>assets/js/dataTables.bootstrap4.min.js'>
</script> -->
<script type="text/javascript" src='<?php echo base_url(); ?>assets/js/custom/magsaysay-library.js'>
</script>
<script type="text/javascript">

    $("#txtSearchBar-sm").val($.cookie("search"))
    console.log($.cookie("search"))
    var selectedradio = $.cookie("radio")
    // console.log(selectedradio )
    if (selectedradio == $("#filter-1").val()) {
        $("#filter-1").attr('checked', 'checked');
    }else if (selectedradio == $("#filter-2").val()) {
        $("#filter-2").attr('checked', 'checked');
    }else if (selectedradio == $("#filter-3").val()) {
        $("#filter-3").attr('checked', 'checked');
    }
    $( window ).ready(function(){
        setTimeout(function() {
            $.magsaysay.library.attachEventsToMaterials();
        },100);
    })
    function myFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("txtSearchBar-sm");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        td1 = tr[i].getElementsByTagName("td")[3];
        td2 = tr[i].getElementsByTagName("td")[4];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }

</script>

</body>

</html>