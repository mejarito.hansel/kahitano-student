<?php $this->load->view('templates/navbartop.php'); ?>
<div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Fullname</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm" id="textFullname" placeholder="Fullname">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Email Address</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm" id="" placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Gender</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                            <label class="form-check-label small" for="inlineRadio1">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label small" for="inlineRadio2">Female</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Birthdate</label>
                        <div class="col-sm-10">
                        <input type="text" class="form-control form-control-sm" id="" placeholder="10/15/1994" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnSave" type="button" class="btn btn-sm btn-block btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-top:100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="card profile text-center" style="width: 18rem;">
                    <span class="mini-button">
                    <img src="<?php echo base_url(); ?>/assets/img/sample-picture.png" style="width:200px; height:200px;" class="rounded-circle mx-auto d-block" alt="Cinque Terre">
                </span>
                      <div class="card-body">
                        <h5 class="card-title" id ="txtStudentName"></h5>
                        <p class="card-text small">
                            Program:
                            <span id="txtCourseP"></span>
                        </p>
                        <p class="card-text small">Cadet No: <span id="txtStudentId"></span></p>
                        <hr>
                       <!--  <a href="#" id="btnEditProfile" class="btn btn-sm btn-block btn-primary">Edit Profile</a> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
            	<div class="col-lg-12">
                    <?php $this->load->view('templates/menubar.php'); ?>
                </div>
                <div class="col-lg-12" style="margin-top:20px; padding-top:20px;">
                	<h4 class="text-uppercase section-header">View Grade's</h4>
                    <label>Select Year and Semester</label>
                    <select class="form-control pull-right" id="SemofStudent">
                        <option></option>

                    </select>
                    <br>
                	<div class="card" style="padding:0px 30px 20px 30px !important;" id="right-section-body">
                		<div class="table table-responsive table-hover">
                			<table class="table table-striped" id="SubjectEncodedTable">
                				<thead>
                					<tr>
                						<th>Course</th>
                						<th>Course Code</th>
                                        <th>Grades Mid Term</th>
                						<th>Final Grades</th>
                                        <th>Year-Block</th>
                					</tr>
                				</thead>
                				<tbody>
                				</tbody>
                			</table>
                		</div>
                	</div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<?php $this->load->view('templates/footer.php'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/DataTables1/1.10.15/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    $.magsaysay.portal.GetInfo();
    $.magsaysay.portal.GetAdditionalInfo();
    $.magsaysay.portal.GetCurriculum();
    $.magsaysay.portal.GetSemofStudents();
    $.magsaysay.portal.GetEncodedGrades();
</script>
