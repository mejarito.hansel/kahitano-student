<?php $this->load->view('templates/navbartop.php'); ?>
<div class="container-fluid" style='margin-top:10rem;'>
   <div class="container">
      <div class="row">
         <div class="col-lg-7 text-center" style="margin:0 auto;">
            <div class="row">
               <div class="col-lg-12">
                  <h1 class='magsaysay-font-color' style="font-family:'Times New Roman','serif';">MMMA E-Library</h1>
                  <div class="form-group">
                     <label for="txtSearchBar">
                     </label><input id="txtSearchBar" type="text" class="form-control text-center" placeholder='Search by Book Name, Book Author, Book Type'>
                  </div>
                  <div class="form-group">
                     <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name='filterby' id="filter-1" value="BName">
                        <label class="form-check-label text-muted" for="filter-1"> Filter by Book Name</label>
                     </div>
                     <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name='filterby' id="filter-2" value="BAuthor">
                        <label class="form-check-label text-muted" for="filter-2"> Filter by Book Author</label>
                     </div>
                     <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name='filterby' id="filter-3" value="BType">
                        <label class="form-check-label text-muted" for="filter-3"> Filter by Book Type</label>
                     </div>
                  </div>
                  <div class="form-group col-lg-6 mt-5" style="margin:0 auto;">
                     <input type="submit" class="btnSearchBook btn btn-block btn-lg btn-primary" value="Search Book">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php $this->load->view('templates/footer.php'); ?>
<script type="text/javascript">
    $(".btnSearchBook").on("click", function(){

      $.cookie("search",$("#txtSearchBar").val())
      console.log($.cookie("search"))
      $.cookie("radio",$("input[name='filterby']:checked"). val())
      console.log($.cookie("radio"))
      window.location.href = "library_search";

    });
        
</script>

</body>
</html>