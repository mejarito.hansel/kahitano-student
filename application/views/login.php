<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>MOL Magsaysay Maritime Academy</title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/icon.png" type="image/gif" sizes="16x16">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.min.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    	<script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.js"></script>
    </head>
<body>

<div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                	<div id="loginAlert" class="alert alert-danger alert-dismissible fade show small" role="alert">
                		Invalid Username/Password
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
					<div class="form-group row">
						<label for="colFormLabelSm" class="labelText col-sm-3 small col-form-label col-form-label-sm">Student No</label>
						<div class="col-sm-10">
						<input type="text" class="form-control form-control-sm" id="textFullname" placeholder="Student ID" autocomplete="off" required>
						</div>
					</div>
					<!-- <div class="form-group row">
						<label for="colFormLabelSm" class="labelText col-sm-3 small col-form-label col-form-label-sm">Birthdate</label>
						<div class="col-sm-10">
						<input type="date" class="form-control form-control-sm" id="txtDate"  placeholder="Birthdate">
						</div>
					</div> -->
					<div class="form-group row">
						<label for="colFormLabelSm" class="labelText col-sm-3 small col-form-label col-form-label-sm">Password</label>
						<div class="col-sm-10">
						<input type="password" class="form-control form-control-sm" id="txtPassword" autocomplete="new-password" placeholder="Password">
						</div>
					</div>
					<div id = "loginSuccess" class="alert alert-success alert-dismissible fade show small" role="alert"><span><i class="fa fa-check-circle"></i> Success! Login Successful!</span></div>
                </div>
            </div>
            <div class="modal-footer">
            	<!-- <form action="http://localhost/studentportal/home"> -->
                	<input id="btnLogin" type="submit" class="btn btn-sm btn-block btn-primary" value="Login">
            	<!-- </form> -->
            </div>
        </div>
    </div>
</div>
<nav class="header">
	<div class="container">
		<div class="main-nav">
			<nav class="navbar navbar-fixed-top">
				<img class="navbar-logo" src="<?php echo base_url(); ?>/assets/img/logo.png" alt="logo">
				<div class="form-inline my-2 my-lg-0">
					<div class="btn-group" role="group" aria-label="Basic example">
					<button type="button" class="btn btn-secondary btn-link">HOME</button>
					<button type="button" class="btn btn-secondary btn-link">ABOUT THE ACADEMY</button>
					<button type="button" class="btn btn-secondary btn-link">YOUR JOURNEY</button>
					<button type="button" class="btn btn-secondary btn-link">CONTACT</button>
					<button id="login" type="button" class="btn btn-secondary btn-link">LOGIN</button>
					</div>
				</div>
			</nav>
		</div>
	</div>
</nav>
<img class='img-fluid' style="margin-top:70px;" src="<?php echo base_url(); ?>/assets/img/background.png">

<?php $this->load->view('templates/footer.php'); ?>
<script type="text/javascript">
	$("#login").on("click", function(){
		$("#mymodal").modal("show");
	});

</script>
<script type="text/javascript">
	$.magsaysay.portal.Authentication();
</script>
</body>
</html>