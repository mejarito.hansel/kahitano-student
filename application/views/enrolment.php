<?php $this->load->view('templates/navbartop.php'); ?>
<div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Fullname</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="textFullname" placeholder="Fullname">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Email Address</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="" placeholder="Email Address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Gender</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                            <label class="form-check-label small" for="inlineRadio1">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label small" for="inlineRadio2">Female</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="colFormLabelSm" class="col-sm-2 small col-form-label col-form-label-sm">Birthdate</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="" placeholder="10/15/1994" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnSave" type="button" class="btn btn-sm btn-block btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" style="margin-top:100px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="card profile text-center" style="width: 18rem;">
                    <span class="mini-button">
                    <img src="<?php echo base_url(); ?>/assets/img/sample-picture.png" style="width:200px; height:200px;" class="rounded-circle mx-auto d-block" alt="Cinque Terre">
                </span>
                    <div class="card-body">
                        <h5 class="card-title" id ="txtStudentName"></h5>
                        <p class="card-text small">
                            Program:
                            <span id="txtCourseP"></span>
                        </p>
                        <p class="card-text small">Cadet Id: <span id="txtStudentId"></span></p>
                        <hr>
                       <!--  <a href="#" id="btnEditProfile" class="btn btn-sm btn-block btn-primary">Edit Profile</a> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-8">

                <div class="col-lg-12">
                    <?php $this->load->view('templates/menubar.php'); ?>
                </div>

                <div class="col-lg-12" style="margin-top:20px; padding-top:20px;">
                    <h4 class="text-uppercase section-header">PRE-REGISTRATION ENROLMENT </h4><br>
                    <div id="txtMessage">
                        <h4>IMPORTANT!</h4>
                        <p class="text-muted small">Your details has been submitted to our Admission Office! Please visit MOL MAGSAYSAY MARITIME ACADEMY from Mon-Fri, 8am-5pm and Sat, 8am-12nn to confirm your registration. Bring ID and Php 500 registration fee. Other requirements
                            could be to follow. See you!</p>
                    </div>
                    <div class="card" style="padding:0px 30px 20px 30px !important;" id="right-section-body">


                        <div class="row">

                            <div id="accordion" style="width:100% !important;">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link float-left text-left" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                      Requirements
                                    </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <h4>FRESHMEN (NEW ENROLLEES)</h4>
                                            <ol>
                                                <li>Accomplished Application Form</li>
                                                <li>High School Report Card (Form 138)</li>
                                                <li>High School Transcript (Form 137a)</li>
                                                <li>Note: This should be a school to school transaction to ensure its authenticity</li>
                                                <li>Certificate of Good Moral Character</li>
                                                <li>2×2 picture (for 201 file)</li>
                                                <li>1×1 picture (for ID)</li>
                                            </ol>
                                            <h4>FRESHMEN (NEW ENROLLEES)</h4>
                                            <ol>
                                                <li>Accomplished Application Form</li>
                                                <li>Accomplished Application Form</li>
                                                <li>Honorable Dismissal/ Transfer Credentials</li>
                                                <li>Temporary Transcript/ Certificate of Grades for evaluation purposes</li>
                                                <li>Official Transcript of Records to be sent by the previous school with a notation:</li>
                                                <li>COPY FOR: AMA University, Proj. 8, QC</li>
                                                <li>Subject description taken from the previous school</li>
                                                <li>1×1 picture (for ID)</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed  float-left text-left" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      Procedure
                                    </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            <h6>STEP 1<br><span class="small text-muted">Apply for the entrance exam for freshmen and qualifying exam for transferees</span></h6><br>
                                            <h6>STEP 2<br><span class="small text-muted">Proceed to the College Dean’s Office for interview</span></h6><br>
                                            <h6>STEP 3<br><span class="small text-muted">After the interview, pay the necessary fees at the Cashier</span></h6><br>
                                            <h6>STEP 4<br><span class="small text-muted">Present the proof of payment to the Dean of the College for the scheduling of the entrance examination/qualifying examination</span></h6><br>
                                            <h6>STEP 5<br><span class="small text-muted">After passing the entrance examination, proceed to the Registrar’s Office for enlistment of the subjects to be taken during the semester</span></h6><br>
                                            <h6>STEP 6<br><span class="small text-muted">After paying the fees and enlistment of subjects, you will be officially enrolled</span></h6><br>
                                            <h6>STEP 7<br><span class="small text-muted">Get a copy of your registration form which shows all the subjects that you will need to take during the semester/trimester</span></h6>

                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed float-left text-left" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Policy
                                    </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            <p><strong>Lost High School Card</strong></p>
                                            <p>Upon admission if the High School Card is lost require the student to get a certification from the school that his/her High School Transcript has not been sent to any school for College Admission together with
                                                the 2nd copy of her high school card (Form 138).</p>
                                            <p><strong>Crediting of Subjects</strong></p>
                                            <ul>
                                                <li>Subjects to be credited should have the same subject description, scope of study and of the same units.</li>
                                                <li>Crediting of subjects should be done upon admission of the student.</li>
                                                <li>General subjects are automatically credited such as English, Filipino, Sociology, etc.</li>
                                            </ul>
                                            <p><strong>Adding Subjects</strong></p>
                                            <ul>
                                                <li>Secure from the Registrar’s Office the Dropping/Adding form.</li>
                                                <li>Fill-out the form properly – your name, student number, course and indicate the SY.</li>
                                                <li>Proceed to the Dean’s Office for approval and endorsement to the Registrar.</li>
                                                <li>Submit the form to the Registrar for approval and encoding.</li>
                                                <li>Get the Students copy and attached it to your Registration form.</li>
                                            </ul>
                                            <p><strong>Shifting to another course</strong></p>
                                            <ul>
                                                <li>Secure from the Registrar’s Office the Change of Course form.</li>
                                                <li>Fill-out the form properly – your name, student number, course and indicate the SY.</li>
                                                <li>Proceed to the Dean’s Office for approval and endorsement to the Registrar</li>
                                                <li>Submit the form to the Registrar for approval and encoding.</li>
                                                <li>Get the Students copy and attached it to your Registration form.</li>
                                            </ul>
                                            <p><strong>NFE/ INC Grades</strong></p>
                                            <ul>
                                                <li>Submit your Letter of request to the Dean/School Director of your College for approval.</li>
                                                <li>After the approval, proceed to the Accounting or Cashiers for payment.</li>
                                                <li>Proceed to the Registrar’s Office and secure the Completion form.</li>
                                                <li>Fill-out the form and submit it to the Faculty member for scheduling of exams.</li>
                                            </ul>
                                            <p><strong>After the exams, the faculty member would submit the Grading Sheet with the following attachments:</strong><br /> – The Letter of request of the student<br /> – Proof of Payment<br /> – Completion Form<br
                                                /> – Spreadsheet</p>
                                            <p><strong>Prerequisites</strong></p>
                                            <p>Only graduating students are allowed to enroll prerequisite subjects simultaneously within the semester wherein the student will be required to sign the waiver form which is at the Registrar’s Office. Major
                                                subjects are not applicable on this case.<br /> Example: English 2 is a prerequisite subject of English 3. If the student fails English 3, he/she automatically fails English 2 even if he/she gets a passing
                                                grade.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row col-lg-12" style="margin-top:50px;">
                                <p class="font-weight-bold">Personal Information <span class="text-danger">*</span></p>
                            </div>
                            <div class="row col-lg-12">
                                <p class="small font-weight-bold">Fullname</p>
                            </div>
                            <div class="form-row" style="width:100%;">
                                <div class="col-lg-4">
                                    <input type="text" class="form-control form" placeholder="First Name">
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control form" placeholder="Middle Name">
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control form" placeholder="Last Name">
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="row col-lg-12">
                                <p style="line-height:0px !important;" class="small font-weight-bold">Gender <span class="text-danger">*</span></p>
                            </div>
                            <div class="form-row" style="width:100%; padding-bottom:0px !important; line-height: 0px !important;">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label small" for="inlineRadio1">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                    <label class="form-check-label small" for="inlineRadio2">Female</label>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row col-lg-12">
                                <p class="small font-weight-bold">Contact Information <span class="text-danger">*</span></p>
                            </div>
                            <div class="form-row" style="width:100%;">
                                <div class="col-lg-12">
                                    <input type="text" class="form-control form" placeholder="Mobile Number">
                                </div>
                                <br>
                                <br>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control form" placeholder="Home Number">
                                </div>
                            </div>
                            <br>
                            <div class="row col-lg-12" style="margin-top:15px">
                                <p class="small font-weight-bold">Choose Type <span class="text-danger">*</span></p>
                            </div>
                            <div class="col-lg-12">
                                <select class="custom-select select">
                                    <option selected>-- Select Course --</option>
                                    <option value="1">Transferee</option>
                                    <option value="2">New Student</option>
                                    <option value="3">Old Student</option>
                                </select>
                            </div>
                            <br>
                            <div class="row col-lg-12" style="margin-top:15px">
                                <p class="small font-weight-bold">Choose Course <span class="text-danger">*</span></p>
                            </div>
                            <div class="col-lg-12">
                                <select class="custom-select select">
                                    <option selected>-- Select Course --</option>
                                    <option value="1">Bachelor of Science in Information Technology</option>
                                    <option value="2">Bachelor of Science in Inforation System</option>
                                    <option value="3">Bachelor of Science in Computer Science</option>
                                </select>
                            </div>
                            <div class="container-fluid">
                                <button id="btnSubmitPreEnrol" style="margin-top:20px;" class="btn btn-secondary btn-sm btn-primary float-right">Submit</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('templates/footer.php'); ?>
<script type="text/javascript">
    $.magsaysay.portal.GetInfo();
    $.magsaysay.portal.GetAdditionalInfo();
    $.magsaysay.portal.GetCurriculum();
</script>