<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>MOL Magsaysay Maritime Academy</title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/icon.png" type="image/gif" sizes="16x16">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.min.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">       
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables1/1.10.15/media/css/jquery.dataTables.css"/>
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.js"></script>
    </head>
<body>
	<?php $this->load->view('templates/preloader'); ?>
	<?php 	if (!isset($_COOKIE["stud_id"])){
		redirect('PageController');
	}?>
<nav class="header">
	<div class="main-nav">
		<div class="container">
			<nav class="navbar navbar-fixed-top">
				<img class="navbar-logo" src="<?php echo base_url(); ?>/assets/img/logo.png" alt="logo">
				<div class="form-inline my-2 my-lg-0">
					<div class="btn-group" role="group" aria-label="Basic example">
						<a  class="btn btn-secondary btn-link" href="home">News &amp; Announcements<span class="sr-only">(current)</span></a>
						<a class="btn btn-secondary btn-link dropdown-toggle" href='' id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Course Record</a>
						  <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" style='left:47.5%'>
                                        <li><a class="dropdown-item" href="viewschedule">Class Schedule</a></li>
										<li><a class="dropdown-item" href="courseoutline">Course Curriculum</a></li>
                                        <li><a class="dropdown-item" href="viewgrades">View Grades</a></li>
							</ul>
	<!-- 				<a class="btn btn-secondary btn-link">Course Curriculum</a>
					<a class="btn btn-secondary btn-link" href="viewgrades">View Grades</a> --> 
						<a  class="btn btn-secondary btn-link" href="library">Library</a>
						<a  class="btn btn-secondary btn-link" href="#!" data-target="#exampleModal" data-toggle="modal" id="change">Change Password</a>

					<a href='logout' class="btn btn-secondary btn-link">LOGOUT</a>
					
					</div>
				</div>
			</nav>
		</div>
	</div>
</nav>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="alert alert-danger hide" id="icpID"><strong>Error!</strong> Internet connection problem</div>
      	<div class="alert alert-success hide" id="success"><strong>Success!</strong> Successfully updated password!</div>
      	<div class="alert alert-danger hide" id="requiredfield"><strong>Error!</strong> Required all fields</div>
      	<div class="alert alert-danger hide" id="passwordmatch"><strong>Error!</strong> Password didn't match</div>
        Password must have:<br><br>
                  <span class="strengthHelp"><small>Have at least 8 characters</small></span><br>
                  <span class="strengthHelp"><small>Include 1 capital letter and 1 number</small></span><br>
                  <span class="strengthHelp"><small>Include 1 special character</small></span>
        <br>
        <br>
        <div class="form-group">
        	<label>New Password</label>
        	<input type="password" name="" class="form-control" id="newPass" placeholder="New Password" onkeyup="validatePassword(this.value)">
        </div>
        <span id="strength"></span>

                              <div class="progress" hidden>
                        <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                          <span class="sr-only">80% Complete (danger)</span>
                        </div>
                      </div>
                        <span id="msg"></span>
		  <div class="form-group">
		    <label>Retype New Password</label>
        	<input type="password" name="" class="form-control" id="retypePass" placeholder="Retype New Password">
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
        <button type="button" class="btn btn-primary" id="saveBtn">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src='<?php echo base_url(); ?>assets/js/custom/magsaysay-library.js'>
</script>
<script>
            function validatePassword(password) {
                if($("#newPass").val().length > 7){
                  $(".strengthHelp").eq(0).css("color","green");
                }else{
                  $(".strengthHelp").eq(0).css("color","black");  
                }
                // Do not show anything when the length of password is zero.
                if (password.length === 0) {
                    document.getElementById("msg").innerHTML = "";
                    $(".progress-bar").css("width","0%");
                    $(".progress").attr("hidden","hidden");
                    return;
                }
                // Create an array and push all possible values that you want in password
                var matchedCase = new Array();
                matchedCase.push("[$@$!%*#?&]"); // Special Charector
                matchedCase.push("[A-Z]");      // Uppercase Alpabates
                matchedCase.push("[0-9]");      // Numbers
                matchedCase.push("[a-z]");     // Lowercase Alphabates

                // Check the conditions
                var ctr = 0;
                for (var i = 0; i < matchedCase.length; i++) {
                    if (new RegExp(matchedCase[i]).test(password)) {
                        ctr++;
                    }
                }
                // Display it
                var color = "";
                var strength = "";
                switch (ctr) {
                    case 0:
                    case 1:
                    case 2:
                      console.log("Working")
                        strength = "Very Weak";
                        color = "red";
                        $(".progress").removeAttr("hidden")
                        $(".progress-bar").css("width","10%");
                        if($("#newPass").val().length > 4){
                          $(".progress-bar").css("width","20%");
                          $(".strengthHelp").eq(1).css("color","black");
                          $(".strengthHelp").eq(2).css("color","black");
                        }
                        $(".progress-bar").removeClass("progress-bar-info");
                        $(".progress-bar").removeClass("progress-bar-success").addClass("progress-bar-danger");
                        break;
                    case 3:
                        strength = "Medium";
                        color = "orange";
                        $(".progress").removeAttr("hidden");
                        $(".progress-bar").css("width","40%");
                        if($("#newPass").val().length > 1){
                          $(".progress-bar").css("width","60%");
                          $(".strengthHelp").eq(1).css("color","green");
                        }else if($("#newPass").val().length > 0){
                          $(".strengthHelp").eq(1).css("color","black");
                        }else{
                          $(".strengthHelp").eq(1).css("color","black");
                        }
                        $(".progress-bar").removeClass("progress-bar-danger");
                        $(".progress-bar").removeClass("progress-bar-success").addClass("progress-bar-info");
                        break;
                    case 4:
                        strength = "Strong";
                        color = "green";
                        $(".progress").removeAttr("hidden");
                        $(".progress-bar").css("width","80%");
                        if($("#newPass").val().length > 10){
                          $(".progress-bar").css("width","100%");
                          $(".strengthHelp").eq(2).css("color","green");
                        }
                        $(".progress-bar").removeClass("progress-bar-danger");
                        $(".progress-bar").removeClass("progress-bar-info").addClass("progress-bar-success");
                        break;
                }
                document.getElementById("msg").style.color = color;
            }
        
             $(document).ready(function() {
            
          $(function() {
              $('#saveBtn').attr('disabled', 'disabled');
          });
      
          $('input[type=password]').keyup(function() {
                  
              if ($('#newPass').val() !=''&&
                $('#retypePass').val() != ''){
 
                 if (($('#newPass').val()!="") && ($('#retypePass').val()!="")){

                   if (($('#newPass').val())==($('#retypePass').val()))
                      {
                      $('#saveBtn').removeAttr('disabled');
                    }

                   }

                }

               else {
                  $('#saveBtn').attr('disabled', 'disabled');
              }
          });
        });
      </script>
<script type="text/javascript">
    $.magsaysay.library.changePass();
</script>