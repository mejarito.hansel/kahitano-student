<script>
$(window).on("load",function() {
  	$(".loaderDiv").fadeOut("slow"); // loading DIV fades out 
});
</script>
<div class="loaderDiv" style="z-index:9999; position:absolute; top:0px; left:0px; width:100%; height:100%; background:rgba(255,255,255,0.9);">
	<img src="<?php echo base_url(); ?>assets/img/preloader.gif" style="position: absolute; width:100px; top: 50%; left: 50%; transform: translate(-50%, -50%);">
</div>