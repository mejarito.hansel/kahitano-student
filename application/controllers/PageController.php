<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageController extends CI_Controller {

	public function index() {
		$this->load->view('login');
	}
	
	public function library() {
		if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('library');
	}
	
	public function library_search() {
		if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('library_search');
	}
	
	public function enrolment() {
		if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('enrolment');
	}

	public function home() {
			if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('home');
	}

	public function courseoutline() {
			if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('courseoutline');
	}

	public function viewgrades() {
			if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('viewgrades');
	}
	public function viewschedule() {
			if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('viewschedule');
	}
	public function payment() {
			if (!isset($_COOKIE["stud_id"])){
			$this->load->view('login');
		}
		$this->load->view('payment');
	}

	public function logout(){
			unset($_COOKIE['stud_id']);
			unset($_COOKIE['student_id']);
			setcookie('stud_id', null, -1, '/');
    		setcookie('student_id', null, -1, '/');
			$this->session->sess_destroy();
			redirect('PageController');
	}

}